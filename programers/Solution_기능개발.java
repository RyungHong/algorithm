package programers;

import java.util.ArrayList;

public class Solution_기능개발 {
    public  int[] solution(int[] progresses, int[] speeds) {
        int[] answer = getFounctonCount(progresses, speeds);

        return answer;
    }
    public int[] getFounctonCount(int[] progresses, int[] speeds) {
        int[] finDates = new int[progresses.length];
        int willProgress = 0;
        for(int idx = 0; idx < progresses.length; idx++) {
            willProgress = 100 - progresses[idx];
            if(willProgress % speeds[idx] == 0 ) {
                finDates[idx] = willProgress / speeds[idx];
            } else {
                finDates[idx] = (willProgress / speeds[idx]) + 1;
            }
        }
        ArrayList<Integer> answer = new ArrayList<>();
        int count = 1;
        int finDate = finDates[0];
        for(int idx = 1; idx < finDates.length; idx++) {
            if(finDate >= finDates[idx]) {
              count++;
            } else {
                finDate = finDates[idx];
                answer.add(count);
                count = 1;
            }
        }
        answer.add(count);

        return convertToArray(answer);
    }

    private int[] convertToArray(ArrayList<Integer> list) {
        int[] array = new int[list.size()];
        for(int idx = 0; idx < list.size(); idx++) {
            array[idx] = list.get(idx);
        }
        return array;
    }
}
//class Main{
//    public static void main(String[] args) {
//        Solution s = new Solution();
//        int[] ans = s.solution(new int[]{93, 30, 55}, new int[]{1,30,5});
//        System.out.println(ans.toString());
//    }
//}