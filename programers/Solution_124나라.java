package programers;

public class Solution_124나라 {
    public String solution(int n) {
        String answer = "";
        int r;

        if(n < 3) {
            answer = n + "";
        } else if(n == 3) {
            answer = 4 + "";
        } else {
           while(n >= 3) {
               r = n % 3;
               if(r != 0) {
                   answer = r + answer;
                    n /= 3;
               } else {
                   answer = "4" + answer;
                    n = (n / 3) - 1;
               }
           }
           if(n != 0) {
               answer = n + answer;
           }

        }

        return answer;
    }
}
//class Main {
//    public static void main(String[] args) {
//        Solution_124나라 s = new Solution_124나라();
//        System.out.println(s.solution(10));
//    }
//}