package basic;
/*
        - union - find 알고리즘 사용
        - Disjoint set 자료구조 사용
            정의 : 서로 중복되지 않는 부분 집합들 로 나눠진 원소들에 대한 정보를 저장하고 조작하는 자료구조
 */

import java.io.IOException;

public class Main_Union_find {
    public static void main(String[] args) throws IOException {

        int n = 7;
        int[] root = new int[n + 1];
        makeSet(root);

        union(root, 1, 2);

        if (find(root, 1) == find(root, 2)) {
            System.out.println("동일한 집합");
        } else {
            System.out.println("서로 다른 집");
        }

    }

    private static void union(int[] root, int a, int b) {
        int rootA = find(root, a);
        int rootB = find(root, b);

        if (rootA == rootB) {
            return;
        } else {
            root[rootA] = rootB;
        }
    }

    private static int find(int[] root, int a) {
        if (root[a] == a)
            return a;
        else
            return root[a] = find(root, root[a]);
    }

    private static void makeSet(int[] arr) {
        for (int idx = 0; idx < arr.length; idx++) {
            arr[idx] = idx;
        }
    }
}
