package basic;

/*
    TSP 문제
    DP + Bitmask 알고리즘 사용 ...
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;

public class Main_TSP {
    static int[][] distance;
    static int[][] dp;
    static int N;
    static int INF = 10000000;

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer st = new StringTokenizer(br.readLine());
        N = Integer.parseInt(st.nextToken());
        distance = new int[N][N];
        dp = new int[N][(1 << N)];
        for (int row = 0; row < N; row++) {
            st = new StringTokenizer(br.readLine());
            for (int col = 0; col < N; col++) {
                distance[row][col] = Integer.parseInt(st.nextToken());
                if (distance[row][col] == 0) {
                    distance[row][col] = INF;
                }
            }
        }
        for (int n = 0; n < N; n++) {
            Arrays.fill(dp[n], -1);
        }
        int start = 0;
        int ans = backTracking(start, (1 << start));
        System.out.println(ans);
    }
    // start : 현재 방문한 도시
    // visited : 이제까지 방문했던 도시를 비트마스크해서 가지고 있는 정수
    private static int backTracking(int start, int visited) {
        //이전에 방분을 해봤다면
        if (dp[start][visited] != -1) {
            return dp[start][visited];
        }

        //모든 곳을 방분했다면
        if (visited == (1 << N) - 1){
            return dp[start][visited] = distance[start][0] == INF ? INF : distance[start][0];
        }

        dp[start][visited] = INF;
        for(int to = 0; to < N; to++) {
            //distance[start][to] == INF : 현제 도시에서 해당 도시(to)로 갈 수 없을 때
            //(visited & (1<<to)) > 0 : 이미 해당 도시는 방문했을 경우
            if(distance[start][to] == INF || (visited & (1 << to)) > 0) {
                continue;
            }

            dp[start][visited] = Math.min(dp[start][visited],
                    backTracking(to, visited | (1 << to)) + distance[start][to]);
        }
        return  dp[start][visited];
    }
}
