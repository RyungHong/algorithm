package basic;

import java.util.Arrays;
import java.util.Scanner;

public class Main_permitation {
    static int N;
    static boolean[] visitied;
    static int[] result; // visited를 이용할 때 필
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        N = sc.nextInt();
        int[] arr = new int[N];
        result = new int[N];
        visitied = new boolean[N];
        for(int i = 0 ; i < arr.length ; i++){
            arr[i] = i + 1;
        }

        perm(arr, 0);
    }
    public static void perm(int[] arr, int loc) {
        if(loc == arr.length) {
            System.out.println(Arrays.toString(result));
        }
        for(int i = 0 ; i < N; i++) {
            if(visitied[i] == false) {
                visitied[i] = true;
                result[loc] = arr[i];
                perm(arr, loc +1);
                visitied[i] = false;
            }
        }
    }
//    swap 사용한 순열
//    public static void perm(int[] arr, int loc) {
//        if(loc == arr.length) {
//            System.out.println(Arrays.toString(arr));
//            return;
//        }
//
//        for(int idx = loc ; idx < arr.length ; idx++){
//            swap(arr, idx, loc);
//            perm(arr, loc + 1);
//            swap(arr, idx, loc);
//        }
//    }

//    public static void swap(int[] arr, int idx, int loc) {
//        int temp = arr[idx];
//        arr[idx] = arr[loc];
//        arr[loc] = temp;
//    }
}
