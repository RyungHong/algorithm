package basic;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Main_combination {
    static int N, R;
    static boolean[] visitied;
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer st = new StringTokenizer(br.readLine());
        N = Integer.parseInt(st.nextToken());
        R = Integer.parseInt(st.nextToken());
        visitied = new boolean[N];
        int[] arr = new int[N];
        for(int i = 0 ; i < N ; i++) {
            arr[i] = i+1;
        }

        comb(arr, 0, 0);
    }

    private static void comb(int[] arr, int depth, int r) {
        if(r == R) {
            for(int i = 0 ; i < N ; i++){
                if(visitied[i]) {
                    System.out.print(arr[i]);
                }
            }
            System.out.println();
            return;
        }

        if(depth == N) {
            return;
        }

        visitied[depth] = true;
        comb(arr, depth + 1, r +1);

        visitied[depth] = false;
        comb(arr, depth + 1, r);
    }
}
