package baekjoon;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.Buffer;
import java.util.StringTokenizer;
/*
백준 문제 사이트 1149번 문제
RGB 거리
 */
public class Main_1149 {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer st = new StringTokenizer(br.readLine());
        int N = Integer.parseInt(st.nextToken());
        int[][] map = new int[N][N];
        int[][] result = new int[N][N];
        for(int row = 0 ; row < N ; row++) {
            st = new StringTokenizer(br.readLine());
            for(int col = 0 ; col < 3; col++ ){
                map[row][col] = Integer.parseInt(st.nextToken());
            }
        }
        for(int col = 0 ; col < 3 ; col++) {
            result[0][col] = map[0][col];
        }
        for(int row = 1 ; row < N ; row++) {
            result[row][0] = map[row][0] + Math.min(result[row-1][1], result[row-1][2]);
            result[row][1] = map[row][1] + Math.min(result[row-1][0], result[row-1][2]);
            result[row][2] = map[row][2] + Math.min(result[row-1][0], result[row-1][1]);
        }

        int ans = Integer.MAX_VALUE;
        for(int col = 0 ; col < 3 ; col++) {
            if(result[N-1][col] < ans) {
                ans = result[N-1][col];
            }
        }
        System.out.println(ans);
    }
}
