package baekjoon;

/*
      백준 2468 안전 영역
        - 브루트 포스 + dfs or bfs 사용
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Main_2468 {
    static int N;
    static int[][] map;
    static int[] drow = {0, 1, 0, -1};
    static int[] dcol = {1, 0, -1, 0};

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer st = new StringTokenizer(br.readLine());

        N = Integer.parseInt(st.nextToken());
        map = new int[N][N];
        int maxLevel = -1;
        for (int row = 0; row < N; row++) {
            st = new StringTokenizer(br.readLine());
            for (int col = 0; col < N; col++) {
                map[row][col] = Integer.parseInt(st.nextToken());
                if (map[row][col] > maxLevel) {
                    maxLevel = map[row][col];
                }
            }
        }

        int ans = solv(maxLevel);
        System.out.println(ans);

    }

    private static int solv(int maxLevel) {
        boolean[][] visited;
        int maxCnt = 0, cnt = 0;
        for (int level = 0; level < maxLevel; level++) {
            visited = new boolean[N][N];
            cnt = 0;
            for (int row = 0; row < N; row++) {
                for (int col = 0; col < N; col++) {
                    if (visited[row][col] || map[row][col] <= level) {
                        continue;
                    }
                    visited[row][col] = true;
                    dfs(row, col, visited, level);
                    cnt++;
                }
            }
            if (maxCnt < cnt) {
                maxCnt = cnt;
            }
        }

        return maxCnt;
    }

    private static void dfs(int row, int col, boolean[][] visited, int level) {

        int trow, tcol;
        for (int dir = 0; dir < 4; dir++) {
            trow = row + drow[dir];
            tcol = col + dcol[dir];
            if (trow < 0 || trow >= N || tcol < 0 || tcol >= N) {
                continue;
            }
            if (visited[trow][tcol]) {
                continue;
            }
            if (map[trow][tcol] <= level) {
                continue;
            }

            visited[trow][tcol] = true;
            dfs(trow, tcol, visited, level);
        }

    }
}
