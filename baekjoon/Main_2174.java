package baekjoon;
/*
    백준 2174 로봇 시뮬레이션

    시뮬레이션
    아이디어 : 좌표를 제가 자주 사용하던 것으로 수정
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.StringTokenizer;

public class Main_2174 {
    static int ROW, COL;
    static int[][] map;
    static int N, M;
    static ArrayList<Pos> robots;
    static LinkedList<Cmd> commands;

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer st = new StringTokenizer(br.readLine());

        COL = Integer.parseInt(st.nextToken());
        ROW = Integer.parseInt(st.nextToken());
        map = new int[ROW + 1][COL + 1];

        st = new StringTokenizer(br.readLine());
        N = Integer.parseInt(st.nextToken());
        M = Integer.parseInt(st.nextToken());
        robots = new ArrayList<>();

        int row, col, dir;
        for (int num = 1; num <= N; num++) {
            st = new StringTokenizer(br.readLine());
            col = Integer.parseInt(st.nextToken());
            row = Integer.parseInt(st.nextToken());
            dir = getDirection(st.nextToken());
            row = ROW + 1 - row;
            map[row][col] = num;
            robots.add(new Pos(num, row, col, dir));
        }

        int robotNum, cnt;
        char operChar;
        commands = new LinkedList<>();
        for (int cmd = 0; cmd < M; cmd++) {
            st = new StringTokenizer(br.readLine());
            robotNum = Integer.parseInt(st.nextToken());
            operChar = st.nextToken().charAt(0);
            cnt = Integer.parseInt(st.nextToken());
            commands.add(new Cmd(robotNum, operChar, cnt));
        }

        solv();


    }

    private static void solv() {
        Cmd commandLine;
        Pos target;
        char operateChar;
        int cnt;
        boolean errorcode = true;
        while (!commands.isEmpty()) {
            commandLine = commands.poll();
            target = robots.get(commandLine.num - 1);
            operateChar = commandLine.operateChar;
            cnt = commandLine.cnt;
            if (operateChar == 'L') {
                turnLeft(target, cnt);
            } else if (operateChar == 'R') {
                turnRight(target, cnt);
            } else {
                errorcode = forward(target, cnt);
                if (!errorcode) {
                    break;
                }
            }

        }
        if (errorcode) {
            System.out.println("OK");
        }
    }

    private static boolean forward(Pos target, int cnt) {
        int[] drow = {0, 1, 0, -1};
        int[] dcol = {1, 0, -1, 0};
        int trow;
        int tcol;
        int dir = target.dir;
        map[target.row][target.col] = 0;
        boolean errorCode = true;
        Pos robot;
        for (int time = 0; time < cnt; time++) {
            trow = target.row + drow[dir];
            tcol = target.col + dcol[dir];
            if (trow < 1 || tcol < 1 || trow > ROW || tcol > COL) {
                System.out.println("Robot " + target.num + " crashes into the wall");
                return errorCode = false;
            }

            if (map[trow][tcol] >= 1) {
                System.out.println("Robot " + target.num + " crashes into robot " + map[trow][tcol]);
                return errorCode = false;
            }
            target.row = trow;
            target.col = tcol;
        }
        map[target.row][target.col] = target.num;
        return true;
    }

    private static void turnRight(Pos target, int cnt) {
        int time = cnt % 4;

        for (int t = 0; t < time; t++) {
            if (target.dir == 3) {
                target.dir = 0;
            } else {
                target.dir++;
            }
        }

    }

    private static void turnLeft(Pos target, int cnt) {
        int time = cnt % 4;
        for (int t = 0; t < time; t++) {
            if (target.dir == 0) {
                target.dir = 3;
            } else {
                target.dir--;
            }
        }
    }


    private static int getDirection(String token) {
        if (token.equals("N")) {
            return 3;
        } else if (token.equals("W")) {
            return 2;
        } else if (token.equals("E")) {
            return 0;
        } else {
            return 1;
        }
    }

    static class Pos {
        int num;
        int row;
        int col;
        int dir;

        public Pos(int num, int row, int col, int dir) {
            this.num = num;
            this.row = row;
            this.col = col;
            this.dir = dir;
        }
    }

    static class Cmd {
        int num;
        char operateChar;
        int cnt;

        public Cmd(int num, char operateChar, int cnt) {
            this.num = num;
            this.operateChar = operateChar;
            this.cnt = cnt;
        }
    }
}
