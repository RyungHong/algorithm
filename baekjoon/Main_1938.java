package baekjoon;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.StringTokenizer;

/*
    통나무 옮기기 백준 1938번
    통나무 중간 순서를 기준으로 BFS를 진행
    방문 여부는 3차원 배열로 [가로/세로][Row][Col] 로 여부 판단......
 */
public class Main_1938 {
    static char[][] map;
    static boolean[][][] visited;
    static int N;
    static int[] drow = {0,1,0,-1};
    static int[] dcol = {1,0,-1,0};
    static Pos start, end;
    static int ans;
    static ArrayList<Pos> startList;
    static ArrayList<Pos> endList;
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer st = new StringTokenizer(br.readLine());
        N = Integer.parseInt(st.nextToken());
        String temp;
        map = new char[N][N];
        visited = new boolean[2][N][N];
        startList = new ArrayList<>();
        endList = new ArrayList<>();

        for(int row = 0 ; row < N ; row++) {
            st = new StringTokenizer(br.readLine());
            temp = st.nextToken();
            map[row] = temp.toCharArray();
            for(int col = 0 ; col < N; col++) {
                if(map[row][col] == 'B') {
                    startList.add(new Pos(row,col));
                } else if(map[row][col] == 'E'){
                    endList.add(new Pos(row,col));
                }
            }
        }

        start = new Pos();
        start.row = startList.get(1).row;
        start.col = startList.get(1).col;
        start.shape = getShape(startList);

        end = new Pos();
        end.row = endList.get(1).row;
        end.col = endList.get(1).col;
        end.shape = getShape(endList);


        bfs();
        System.out.println(ans);
    }

    private static void bfs() {
        LinkedList<Pos> queue = new LinkedList<>();
        queue.add(new Pos(start.row, start.col, start.shape));
        visited[start.shape][start.row][start.col] = true;
        Pos from , to;
        int trow, tcol;

        while(!queue.isEmpty()) {
            from = queue.poll();
            //상 하 좌 우
            for(int dir = 0 ; dir < 4 ; dir++) {
                trow = from.row + drow[dir];
                tcol = from.col + dcol[dir];




                // 0 : 세로 모양 , 1 : 가로 모양
                if(from.shape == 0){
                    if(trow -1  < 0 || trow + 1 >= N || tcol < 0 || tcol >= N) {
                        continue;
                    }
                    if(dir == 0) {
                        if(map[trow][tcol] == '1' || map[trow+1][tcol] == '1' || map[trow-1][tcol] == '1') {
                            continue;
                        }
                    } else if(dir == 1) {
                        if( map[trow +1 ][tcol] == '1') {
                            continue;
                        }
                    }else if (dir == 2) {
                        if(map[trow][tcol] == '1' || map[trow - 1][tcol] == '1' || map[trow+1][tcol] == '1') {
                            continue;
                        }
                    } else{
                        if (map[trow -1][tcol] == '1') {
                            continue;
                        }
                    }

                } else {
                    if(trow < 0 || trow >= N || tcol -1 < 0 || tcol +1 >= N) {
                        continue;
                    }

                    if(dir == 0) {
                        if( map[trow ][tcol + 1] == '1') {
                            continue;
                        }
                    } else if(dir ==1) {
                        if(map[trow][tcol] == '1' || map[trow][tcol+1] == '1' || map[trow][tcol-1] == '1' ) {
                            continue;
                        }
                    } else if(dir == 2 ) {
                        if(map[trow][tcol - 1] == '1') {
                            continue;
                        }
                    } else {
                        if ( map[trow ][tcol] == '1' || map[trow][tcol + 1] == '1' || map[trow][tcol - 1] == '1') {
                            continue;
                        }
                    }

                }
                if(visited[from.shape][trow][tcol]) {
                    continue;
                }

                if(from.shape == end.shape && trow == end.row && tcol == end.col) {
                    ans = from.count + 1;
                    return;
                }

                visited[from.shape][trow][tcol] =true;
                to = new Pos(trow, tcol, from.shape, from.count + 1);
                queue.add(to);
            }

            //회전
            if(from.isTurn(map)) {
                if(from.turn() == end.shape && from.row == end.row && from.col == end.col) {
                    ans = from.count + 1;
                    return;
                }
                if(!visited[from.turn()][from.row][from.col]) {
                    visited[from.turn()][from.row][from.col] = true;
                    to = new Pos(from.row, from.col, from.turn(), from.count+ 1 );
                    queue.add(to);
                }
            }
        }
    }

    private static int getShape(ArrayList<Pos> start) {
        if(start.get(0).col == start.get(1).col) {
            return 0;
        } else {
            return 1;
        }
    }

    static class Pos {
        int row;
        int col;
        int shape;// 0 : 세로 , 1 : 가
        int count;
        public Pos() {
        }

        public Pos(int row, int col, int shape) {
            this.row = row;
            this.col = col;
            this.shape = shape;
            this.count = 0;
        }

        public Pos(int row, int col, int shape, int count) {
            this.row = row;
            this.col = col;
            this.shape = shape;
            this.count = count;
        }

        public Pos(int row, int col) {
            this.row = row;
            this.col = col;
        }

        boolean isTurn(char[][] map) {
            if(this.shape == 0) { // 세로
                if(this.row < 0 ||  this.row >= N || this.col -1 < 0 || this.col +1 >= N) {
                    return false;
                }

                if(map[row][col+1] == '1' || map[row+1][col+1] == '1' || map[row-1][col+1] == '1' ||
                    map[row][col-1] == '1' || map[row-1][col-1] == '1' || map[row+1][col-1] == '1') {
                    return false;
                }

            } else { // 가로
                if(this.row -1  < 0 || this.row + 1 >= N || this.col < 0 || this.col >= N) {
                    return false;
                }

                if(map[row-1][col] == '1' || map[row-1][col+1] == '1' || map[row-1][col-1] == '1' ||
                        map[row + 1][col] == '1' || map[row + 1][col + 1] == '1' || map[row + 1][col - 1] == '1') {
                    return false;
                }
            }
            return true;
        }

        int turn() {
            if(this.shape == 1) {
                return 0;
            } else {
                return 1;
            }
        }
    }
}