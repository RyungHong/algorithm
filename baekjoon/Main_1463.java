package baekjoon;
/*
    백준 1463번 1로 만들기
        다이나믹 프로그래
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;

public class Main_1463 {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer st = new StringTokenizer(br.readLine());
        int N = Integer.parseInt(st.nextToken());

        int[] count = new int[N+1];
        Arrays.fill(count, 1000000);
        getOperattionCount(N, count , 0 );
        System.out.println(count[1]);
    }

    private static void getOperattionCount(int n, int[] count, int depth) {
        if(count[n] <=  depth) return;;
        count[n] = depth;

        if(n % 3 == 0) {
            getOperattionCount(n / 3, count, depth + 1);
        }

        if(n % 2 == 0) {
            getOperattionCount(n / 2, count, depth +1);
        }

        if(n != 1) {
            getOperattionCount(n - 1, count, depth +1);
        }
    }
}
