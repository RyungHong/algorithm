package baekjoon;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.StringTokenizer;

/*
    달이 차오른다. 가자 백준 1194 문제
    BFS + BitMask
 */
public class Main_1194 {
    static int ROW, COL;
    static char[][] map;
    static int[] drow = {0, 1, 0, -1};
    static int[] dcol = {1, 0, -1, 0};

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer st = new StringTokenizer(br.readLine());

        ROW = Integer.parseInt(st.nextToken());
        COL = Integer.parseInt(st.nextToken());
        map = new char[ROW][COL];
        int startRow = -1, startCol = -1;
        for(int row = 0 ; row<ROW ; row++){
            st = new StringTokenizer(br.readLine());
            map[row] = st.nextToken().toCharArray();
            if(startRow == -1 && startCol == -1 ) { //시작 위치를 못 찾았으면
                for (int col = 0; col < COL; col++) {
                    if (map[row][col] == '0') {
                        startRow = row;
                        startCol = col;
                        break;
                    }
                }
            }
        }
        int ans = bfs(startRow, startCol);
        System.out.println(ans);
    }

    private static int bfs(int startRow, int startCol) {
        LinkedList<Pos> queue = new LinkedList<>();
        queue.add(new Pos(startRow, startCol, 0,0));
        boolean[][][] visited = new boolean[64][ROW][COL];
        Pos pre, next;
        int trow, tcol;
        int c = 0;
        char symbol, key;
        while(!queue.isEmpty()) {
            pre = queue.poll();
            for(int dir = 0 ; dir < 4; dir++) {
                trow = pre.row + drow[dir];
                tcol = pre.col + dcol[dir];
                if(trow < 0 || trow >= ROW || tcol < 0 || tcol >=COL) {
                    continue;
                }
                if(visited[pre.keys][trow][tcol]){
                    continue;
                }
                symbol = map[trow][tcol];
                if(symbol == '#') {
                    continue;
                } else if(symbol == '1') {
                    return pre.dist + 1;
                } else if(symbol >= 'A' && symbol<='F') {
                    if((pre.keys & (1 << symbol - 'A')) == 0) {
                        continue;
                    }
                } else if(symbol >= 'a' && symbol <='f') {
                    int addKey = pre.keys | (1 << symbol - 'a');
                    next = new Pos(trow, tcol, addKey, pre.dist + 1);
                    queue.add(next);
                    visited[addKey][trow][tcol] = true;
                    continue;
                }

                next = new Pos(trow, tcol, pre.keys, pre.dist + 1);
                queue.add(next);
                visited[pre.keys][trow][tcol] =true;
            }
        }
        return -1;
    }
    static class Pos{
        int row;
        int col;
        int keys;
        int dist;
        public Pos(int row, int col, int keys, int dist) {
            this.keys = keys;
            this.row = row;
            this.col = col;
            this.dist = dist;
        }
    }

}
