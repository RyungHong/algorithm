package baekjoon;
/*
    백준 2133 타일 채우기
    DP 문제 .... 너무 해맸음...
    점화식
    DP[N] = DP[N-2] * 3 + sum(DP[n-4]*2 + DP[n-6] * 2 + ....+ DP[0] *2   N==1 : 0 , N==2 : 3
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
public class Main_2133 {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int N = Integer.parseInt(br.readLine());

        int ans = 0;
        int[] dp = new int[N + 1];
        dp[0] = 1;
        if (N % 2 == 0) {
            dp[2] = 3;
            for (int n = 4; n <= N; n += 2) {
                dp[n] = dp[n-2] * 3;
                for(int k = n-4 ; k>=0 ; k -=2) {
                    dp[n] += dp[k] *2 ;
                }
            }
            ans = dp[N];
        }
        System.out.println(ans);
    }
}
