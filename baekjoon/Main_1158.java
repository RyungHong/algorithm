package baekjoon;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.StringTokenizer;

/*
    백준 문제 1158 - 요세푸스 순열 (N, K)
    queue 자료구조 이용
 */
public class Main_1158 {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer st = new StringTokenizer(br.readLine());

        int N = Integer.parseInt(st.nextToken());
        int K = Integer.parseInt(st.nextToken());

        LinkedList<Integer> queue = new LinkedList<Integer>();
        LinkedList<Integer> result = new LinkedList<Integer>();
        for(int num = 1 ; num <= N; num++) {
            queue.add(num);
        }

        int flag = 0;
        int value = -1;
        while(!queue.isEmpty()) {
            flag++;
            value = queue.poll();
            if(flag == K) {
                result.add(value);
                flag = 0;
            } else {
                queue.add(value);
            }
        }
        int ans = 0;
        System.out.print("<");
        for(int idx = 0 ; idx < result.size()-1; idx++) {
            ans = result.get(idx);
            System.out.print(ans);
            System.out.print(", ");
        }
        System.out.print(result.get(result.size()-1) + ">");
    }
}
