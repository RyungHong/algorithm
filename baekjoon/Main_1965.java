package baekjoon;

/*
    백준 1965 상자 옮기기.
    - 최장 수열로 알고리즘 + Lower_bound (Binary Search 의 응용 : 찾고자 하는 수보다 같거나 클 경우 반환)
    - Upper_bound는 찾고자 하는 수보다 클 경우 .......
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;


public class Main_1965 {
    public static void main(String[] args) throws NumberFormatException, IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int n = Integer.parseInt(br.readLine());
        StringTokenizer st = new StringTokenizer(br.readLine());

        int[] arr = new int[n];

        int[] dp = new int[n];

        for (int i = 0; i < n; i++) {
            arr[i] = Integer.parseInt(st.nextToken());
        }


        dp[0] = arr[0];
        int idx = 0;
        for (int i = 1; i < arr.length; i++) {
            if (dp[idx] < arr[i]) {
                dp[++idx] = arr[i];
            } else {
                int li = lower_bound(dp, idx, arr[i]);
                dp[li] = arr[i];
            }
        }
        int ans = n;
        System.out.println(Arrays.toString(dp));
        for (int i = 0; i < dp.length; i++) {
            if (dp[i] == 0) {
                ans = i;
                break;
            }
        }

        System.out.println(ans);
    }

    private static int lower_bound(int[] dp, int end, int n) {
        int start = 0;

        while (start < end) {
            int mid = (start + end) / 2;
            if (dp[mid] >= n) {
                end = mid;
            } else {
                start = mid + 1;
            }
        }
        return end;
    }
}
