package baekjoon;
/*
    백준 1520 내리막길
        - DFS + DP
        - visited[trow][tcol] 대신 count[trow][tcol]를 방문여부를 체크 해줬지만
            시간초가 발생 (이유 : 한번 갔던 길이더라도 그 경로가 목적지에 도착할 수 없는 경로라면 0을 반환하여
            이후 다시 여기를 도착하더라도 목적지에 도착 못하는 경로를 다시 진행. 그래서 시간 초과)
            (결과 : visitied 배열을 만들어서 한 번 체크해주는 것이 좋다.)
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Main_1520 {
    static int ROW,COL;
    static int[][] map, count;
    static boolean[][] visited;
    static int[] drow = {0, 1, 0, -1};
    static int[] dcol = {1, 0, -1, 0};

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer st = new StringTokenizer(br.readLine());

        ROW = Integer.parseInt(st.nextToken());
        COL = Integer.parseInt(st.nextToken());

        map = new int[ROW][COL];
        count = new int[ROW][COL];
        visited = new boolean[ROW][COL];
        for(int row = 0 ; row <ROW; row++) {
            st = new StringTokenizer(br.readLine());
            for(int col = 0 ; col < COL; col++ ) {
                map[row][col] = Integer.parseInt(st.nextToken());
            }
        }

        count[ROW-1][COL-1] = 1;
        visited[0][0] =true;
        int ans = dfs(0,0);
        System.out.println(ans);
    }

    private static int dfs(int row, int col) {
        int trow, tcol;
        if(row == ROW-1 && col == COL-1) {
            count[row][col] = 1;
            return 1;
        }
        for(int dir = 0 ; dir <4 ; dir++) {
            trow = row + drow[dir];
            tcol = col + dcol[dir];

            if(trow < 0 || trow >= ROW || tcol < 0 || tcol >= COL) {
                continue;
            }

            if(map[row][col] <= map[trow][tcol]) {
                continue;
            }

            if(visited[trow][tcol]) {
                count[row][col] += count[trow][tcol];
                continue;
            }
            visited[trow][tcol] = true;
            
            count[row][col] += dfs(trow, tcol);
        }
        return count[row][col];
    }
}
