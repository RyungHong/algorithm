package baekjoon;
/*
    백준 1717 집합의 표현
        - union - find 알고리즘 사용
        - Disjoint set 자료구조 사용
            정의 : 서로 중복되지 않는 부분 집합들 로 나눠진 원소들에 대한 정보를 저장하고 조작하는 자료구조
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Main_1717 {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer st = new StringTokenizer(br.readLine());

        int n = Integer.parseInt(st.nextToken());
        int m = Integer.parseInt(st.nextToken());
        int[] root = new int[n+1];
        makeSet(root);
        int a, b, operation;
        for(int num = 0 ; num < m ; num++){
            st = new StringTokenizer(br.readLine());
            operation = Integer.parseInt(st.nextToken());
            a = Integer.parseInt(st.nextToken());
            b = Integer.parseInt(st.nextToken());

            if(operation == 0) {
                union(root, a, b);
            } else {
                if(find(root, a) == find(root, b)) {
                    System.out.println("YES");
                } else {
                    System.out.println("NO");
                }
            }
        }

    }

    private static void union(int[] root, int a, int b) {
        int rootA = find(root, a);
        int rootB = find(root, b);

        if(rootA == rootB) {
            return;
        } else {
            root[rootA] = rootB;
        }
    }

    private static int find(int[] root, int a) {
        if(root[a] == a)
            return a;
        else
            return root[a] = find(root, root[a]);
    }

    private static void makeSet(int[] arr) {
        for(int idx = 0 ; idx < arr.length ; idx++){
            arr[idx] = idx;
        }
    }
}
