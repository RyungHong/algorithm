package baekjoon;

/*
    백준 2583 영역 구하기
    - 좌표 -> 내가 자주 쓰늩 배열로 변환
    - 브루트 포스 + DFS or BFS
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.StringTokenizer;

public class Main_2583 {
    static int ROW, COL, cnt;
    static int[] drow = {0, 1, 0, -1};
    static int[] dcol = {1, 0, -1, 0};
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer st = new StringTokenizer(br.readLine());
        ROW = Integer.parseInt(st.nextToken());
        COL= Integer.parseInt(st.nextToken());
        int k = Integer.parseInt(st.nextToken());
        int row1, row2, col1, col2;
        int[][] map = new int[ROW][COL];

        for (int box = 0; box < k; box++) {
            st = new StringTokenizer(br.readLine());
            col1 = Integer.parseInt(st.nextToken());
            row1 = ROW - Integer.parseInt(st.nextToken());
            col2 = Integer.parseInt(st.nextToken());
            row2 = ROW - Integer.parseInt(st.nextToken());

            for (int row = row2; row < row1; row++) {
                for (int col = col1; col < col2; col++) {
                    map[row][col] = 1;
                }
            }
        }

        solv(map);
    }

    private static void solv(int[][] map) {
        boolean[][] visited = new boolean[ROW][COL];
        ArrayList<Integer> boxs = new ArrayList<>();
        for(int row = 0; row < ROW; row++) {
            for(int col=0; col < COL; col++) {
                if(visited[row][col] || map[row][col] > 0) {
                    continue;
                }
                cnt = 1;
                visited[row][col] =true;
                dfs(row, col, map, visited);
                boxs.add(cnt);
            }
        }
        Collections.sort(boxs);
        System.out.println(boxs.size());
        for(Integer boxCnt : boxs) {
            System.out.print(boxCnt + " ");
        }
    }

    private static void dfs(int row, int col, int[][] map, boolean[][] visited) {
        int trow, tcol;
        for(int dir =0; dir < 4; dir++) {
            trow = row + drow[dir];
            tcol = col + dcol[dir];

            if (trow < 0 || trow >= ROW || tcol < 0 || tcol >= COL) {
               continue;
            }
            if(visited[trow][tcol]) {
                continue;
            }
            if(map[trow][tcol] > 0) {
                continue;
            }
            cnt++;
            visited[trow][tcol] = true;
            dfs(trow, tcol, map, visited);
        }
    }
}
