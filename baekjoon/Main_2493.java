package baekjoon;
/*
    백준 문제 2493 탑
        - stack 사용
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Stack;
import java.util.StringTokenizer;

public class Main_2493 {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer st = new StringTokenizer(br.readLine());
        int N = Integer.parseInt(st.nextToken());
        int[] towers = new int[N];
        st = new StringTokenizer(br.readLine());
        for (int idx = 0; idx < N; idx++) {
            towers[idx] = Integer.parseInt(st.nextToken());
        }
        int[] ans = solve(N, towers);
        for (int towerIdx : ans) {
            System.out.print(towerIdx + " ");
        }
    }

    private static int[] solve(int N, int[] towers) {
        Stack<Tower> stack = new Stack<>();
        int[] ans = new int[N];
        for (int idx = 0; idx < N; idx++) {
            Tower tower = new Tower(towers[idx], idx + 1);
            while (!stack.empty() && stack.peek().height < tower.height) {
                stack.pop();
            }
            if (stack.isEmpty()) {
                ans[idx] = 0;// 0
            } else {
                ans[idx] = stack.peek().idx;
            }
            stack.push(tower);
        }
        return ans;
    }

    static class Tower {
        int height;
        int idx;

        public Tower(int height, int idx) {
            this.height = height;
            this.idx = idx;
        }
    }
}
