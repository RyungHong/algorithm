package baekjoon;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.Buffer;
import java.util.LinkedList;
import java.util.StringTokenizer;

/*
    백준 문제 번호 1012번
 */
public class Main_1012 {
    static int N, ROW, COL, bugNum;
    static boolean[][] map;
    static boolean[][] visited;
    static int[] drow = {0, 1, 0, -1};
    static int[] dcol = {1, 0, -1, 0};
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer st = new StringTokenizer(br.readLine());
        N = Integer.parseInt(st.nextToken());
        int row, col;
        for(int test = 0 ; test < N ; test++){
            st = new StringTokenizer(br.readLine());
            ROW = Integer.parseInt(st.nextToken());
            COL = Integer.parseInt(st.nextToken());
            bugNum = Integer.parseInt(st.nextToken());
            map = new boolean[ROW][COL];
            visited = new boolean[ROW][COL];

            for(int num = 0 ; num < bugNum ; num++){
                st = new StringTokenizer(br.readLine());
                row = Integer.parseInt(st.nextToken());
                col = Integer.parseInt(st.nextToken());
                map[row][col] = true;
            }
            int ans = 0;
            for(row = 0 ; row < ROW; row++) {
                for(col = 0; col <COL; col++){
                    if(visited[row][col] == true) {
                        continue;
                    }
                    if(map[row][col] == false) {
                        continue;
                    }
                    bfs(row, col);
                    ans++;
                }
            }
            System.out.println(ans);


        }
    }

    private static void bfs(int row, int col) {
        LinkedList<Pos> queue = new LinkedList<>();
        visited[row][col] = true;
        queue.add(new Pos(row, col));
        Pos temp;
        int trow, tcol;
        while(!queue.isEmpty()) {
            temp = queue.poll();
            for(int dir = 0; dir <4; dir++) {
                trow = temp.row + drow[dir];
                tcol = temp.col + dcol[dir];

                if(trow < 0 || trow >= ROW || tcol < 0 || tcol >= COL) {
                    continue;
                }
                if (visited[trow][tcol]) {
                    continue;
                }
                if (!map[trow][tcol]) {
                    continue;
                }
                visited[trow][tcol] = true;
                queue.add(new Pos(trow,tcol));
            }
        }
    }
    static class Pos{
        int row;
        int col;
        public Pos(int row, int col) {
            this.row = row;
            this.col = col;
        }
    }
}
