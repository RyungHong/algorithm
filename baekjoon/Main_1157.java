package baekjoon;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class Main_1157 {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        char[] word = br.readLine().toUpperCase().toCharArray();
        Map<Character, Integer> map = new HashMap<Character, Integer>();
        for(int idx = 0 ; idx < word.length ; idx++) {
            if(map.containsKey(word[idx])) {
                map.put(word[idx],map.get(word[idx])+1);
            } else {
                map.put(word[idx], 1);
            }
        }
        ArrayList<Character> keys = new ArrayList<>();
        keys.addAll(map.keySet());
        Collections.sort(keys, new Comparator<Character>() {
            @Override
            public int compare(Character o1, Character o2) {
                return map.get(o2).compareTo(map.get(o1));
            }
        });
        System.out.println(keys);
        if(keys.size() > 1 && map.get(keys.get(0)).equals(map.get(keys.get(1)))) {
            System.out.println("?");
        } else {
            System.out.println(keys.get(0));
        }
    }
}